// HomeWorks.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

class Vector
{
public:
    float x;
    float y;
    float z;
    Vector() { x = 0, y = 0, z = 0; };
    Vector(float _x, float _y, float _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    Vector operator * (float a) const
    {
        return Vector( x * a, y * a, z * a );
    }

    Vector operator - (const Vector& v) const
    {
        return Vector(x - v.x, y - v.y, z - v.z);
    }

    friend std::istream& operator >>(std::istream& input, Vector& v) 
    {
        input >> v.x >> v.y >> v.z;
        return input;
    }
};


int main()
{
    Vector v{ .45f, 3.56f, 2.98f };
    Vector v1 = v * 2.5f;
    Vector v2 = v - v1;
    cin >> v2;

}


